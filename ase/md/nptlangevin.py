import numpy as np

from ase.md.md import MolecularDynamics
import warnings
from ase import units

linalg = np.linalg


class Langevin(MolecularDynamics):
    def __init__(self, atoms, temperature=None, friction=None, timestep=None, 
                 stress=None, cell_mass=None, cell_friction=None, fixcm=True,
                 trajectory=None, logfile=None,
                 loginterval=1, dt=None, append_trajectory=False):
        """Molecular Dynamics object.

        Parameters:

        atoms: Atoms object
            The Atoms object to operate on.

        timestep: float
            The time step in ASE time units.

        trajectory: Trajectory object or str  (optional)
            Attach trajectory object.  If *trajectory* is a string a
            Trajectory will be constructed.  Default: None.

        logfile: file object or str (optional)
            If *logfile* is a string, a file with that name will be opened.
            Use '-' for stdout.  Default: None.

        loginterval: int (optional)
            Only write a log line for every *loginterval* time steps.
            Default: 1

        append_trajectory: boolean
            Defaults to False, which causes the trajectory file to be
            overwriten each time the dynamics is restarted from scratch.
            If True, the new structures are appended to the trajectory
            file instead.

        dt: float (deprecated)
            Alias for timestep.
        """
        
        if dt is not None:
            warnings.warn(
                FutureWarning(
                    'dt variable is deprecated; please use timestep.'))
            timestep = dt
        if timestep is None:
            raise TypeError('Missing timestep argument')

        MolecularDynamics.__init__(self, atoms, timestep, trajectory, logfile,
                                   loginterval,
                                   append_trajectory=append_trajectory)
        if temperature is None:
            raise TypeError('Missing temperature argument')
        
        self.timestep = timestep
        self.temperature = temperature
        self.friction = friction
        self.H0 = np.copy(atoms.cell[:])
        self.F = np.identity(len(self.H0))
        self.Fdot = np.zeros_like(self.H0)
        self.stress = stress
        self.cell_mass = cell_mass
        self.cell_friction = cell_friction
        self.fixcm = fixcm

    def get_f_cell(self,F,Fdot):
        sigma_inst = self.atoms.get_stress(voigt=False,include_ideal_gas=True)
        
        V = self.atoms.get_volume()
        masses = self.atoms.get_masses()
        F_inv = linalg.inv(F)
        F_T = F.transpose()
        F_inv_T = linalg.inv(F_T)
        J = linalg.det(F)
        
        l = Fdot*F_inv
        l_T = l.transpose()

        del_sigma_sum_eles = []
        for a in self.atoms:
            r_a = a.position
            p_a = a.momentum
            
            del_sigma_sum_eles.append(np.tensordot(p_a,l@r_a,axes=0)-np.tensordot(l_T@p_a,r_a,axes=0))
        
        delta_sigma = (1/V)*sum(del_sigma_sum_eles)
        
        S = self.stress
        # f_cell = V*(F*SF_T/J-sigma_inst-delta_sigma-self.cell_friction*Fdot)*F_inv_T
        f_cell = V*(S-sigma_inst-delta_sigma-self.cell_friction*Fdot)*F_inv_T
        #print(f"sigma_inst={sigma_inst/units.GPa}")
        #print(f"S={S/units.GPa}")
        #print(f"delta_sigma={delta_sigma/units.GPa}")
        #print(f"fric={self.cell_friction*Fdot/units.GPa}")

        return f_cell
    
    def irun(self, steps):
        """Run dynamics algorithm as generator. This allows, e.g.,
        to easily run two optimizers or MD thermostats at the same time.

        Examples:
        >>> opt1 = BFGS(atoms)
        >>> opt2 = BFGS(StrainFilter(atoms)).irun()
        >>> for _ in opt2:
        >>>     opt1.run()
        """

        self.max_steps = steps + self.nsteps

        # compute initial structure and log the first step
        self.atoms.get_forces()

        # yield the first time to inspect before logging
        yield False

        if self.nsteps == 0:
            self.log()
            self.call_observers()

        forces = None
        # run the algorithm until converged or max_steps reached
        while not self.converged() and self.nsteps < self.max_steps:
            # compute the next step
            forces = self.step(forces)
            self.nsteps += 1

            # let the user inspect the step and change things before logging
            # and predicting the next step
            yield False

            # log the step
            self.log()
            self.call_observers()

        # finally check if algorithm was converged
        yield self.converged()

    def run(self, steps):
        """Run dynamics algorithm.

        This method will return when the forces on all individual
        atoms are less than *fmax* or when the number of steps exceeds
        *steps*."""

        for converged in self.irun(steps):
            pass
        return converged
    
    def step(self, forces_and_fcell=None):

        atoms = self.atoms
        timestep = self.timestep
        T = self.temperature
        M = self.cell_mass
        lamb = self.friction
        kB = units.kB

        
        p = atoms.get_momenta()
        masses = atoms.get_masses()[:, np.newaxis]

        if forces_and_fcell is None:
            forces = atoms.get_forces(md=True)
            std = np.array(np.sqrt(2*lamb*masses*kB*T/timestep))
            G = np.random.normal(size=forces.shape, loc = 0, scale=std*np.ones(3)[np.newaxis,:])
            if self.fixcm:
                G -= G.mean(axis=0)
                G *= len(atoms)/(len(atoms)-1)
            forces += -lamb*p+G
        
            r = atoms.get_positions()
            f_cell = self.get_f_cell(self.F,self.Fdot)
        else:
            r = atoms.get_positions()
            forces, f_cell = forces_and_fcell

        p += 0.5 * self.dt * forces
        

        # if we have constraints then this will do the first part of the
        # RATTLE algorithm:
        atoms.set_positions(r + self.dt * p / masses)
        if atoms.constraints:
            p = (atoms.get_positions() - r) * masses / self.dt

        # We need to store the momenta on the atoms before calculating
        # the forces, as in a parallel Asap calculation atoms may
        # migrate during force calculations, the momenta need to
        # migrate along with the atoms, and masses have migrated along.
        atoms.set_momenta(p, apply_constraint=False)

        self.F += self.Fdot+(f_cell*self.dt**2)/(2*M)
        self.Fdot += f_cell*self.dt/(2*M)
        #atoms.set_cell(atoms.cell[:]*self.F, scale_atoms=True)
        atoms.set_cell(self.H0*self.F, scale_atoms=False)
        f_cell = self.get_f_cell(self.F,self.Fdot)

        forces = atoms.get_forces(md=True)
        masses = atoms.get_masses()[:, np.newaxis]
        std = np.array(np.sqrt(2*lamb*masses*kB*T/timestep))
        G = np.random.normal(size=forces.shape, loc = 0, scale=std*np.ones(3)[np.newaxis,:])
        if self.fixcm:
            G -= G.mean(axis=0)
            G *= len(atoms)/(len(atoms)-1)
        forces += -lamb*p+G

        # Second part of RATTLE will be done here:
        atoms.set_momenta(atoms.get_momenta() + 0.5 * self.dt * forces)
        self.Fdot += f_cell*self.dt/(2*M)
        return forces, f_cell
